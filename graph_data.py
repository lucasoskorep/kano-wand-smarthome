from glob import glob
from pathlib import Path
import matplotlib.pyplot as plt
import pandas as pd

for folder in glob("data/*"):
    path = Path(folder)
    print(path.stem)
    for c in glob(f"{folder}/*.csv"):
        csv = Path(c)
        print(csv)
        data = pd.read_csv(csv)
        plt.plot(
            data["x"],
            data["y"]
        )
        plt.plot(
            data["pitch"],
            data["roll"]
        )
        plt.title(path.stem)
    plt.show()


for folder in glob("data/*"):
    path = Path(folder)
    for c in glob(f"{folder}/*.csv"):
        csv = Path(c)
        data = pd.read_csv(csv)
        plt.plot(
            data["x"],
            data["y"]
        )
        plt.plot(
            data["pitch"],
            data["roll"]
        )
        plt.title("COMBINED")
plt.show()
